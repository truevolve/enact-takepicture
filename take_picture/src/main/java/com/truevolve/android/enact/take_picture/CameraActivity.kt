package com.truevolve.android.enact.take_picture

import android.graphics.PixelFormat
import android.hardware.Camera
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.ImageView
import android.widget.Toast
import com.truevolve.policy.enact.controllers.R

class CameraActivity : AppCompatActivity(), Camera.PreviewCallback, Camera.PictureCallback {

    private var camera: Camera? = null
    private var cameraConfigured = false
    private var inPreview = false
    private var preview: SurfaceView? = null
    private var previewHolder: SurfaceHolder? = null
    private var snapper: ImageView? = null
    private val TAG = "CameraActivity"
    private val unexpectedTerminationHelper = UnexpectedTerminationHelper()

    private var surfaceCallback: SurfaceHolder.Callback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) {
            // no-op -- wait until surfaceChanged()
        }

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            initPreview(width, height)
            startPreview()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            // no-op
        }
    }

    // Override methods

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_camera)

        preview = findViewById(R.id.surface)
        previewHolder = preview?.holder
        previewHolder?.addCallback(surfaceCallback)

        setupCamera()

        snapper = findViewById(R.id.snapperView)
        snapper?.setOnClickListener { captureImage() }
    }

    override fun onDestroy() {
        //Just in case it wasn't released completely in onPause...
        stopPreviewAndReleaseCamera()
        super.onDestroy()
    }

    override fun onPause() {
        stopPreviewAndReleaseCamera()
        super.onPause()
    }

    override fun onPictureTaken(data: ByteArray, camera: Camera) {
        TakePicture.pictureCallback?.onPictureTakenCallback(data)
        finish()
    }

    override fun onPreviewFrame(data: ByteArray, camera: Camera) {}

    override fun onResume() {
        super.onResume()
        if (camera == null) {
            setupCamera()
        }
    }

    // Override methods
    // Private methods

    private fun captureImage() {
        snapper?.setOnClickListener(null)
        camera?.takePicture(null, null, this)
    }

    private fun getBestPreviewSize(width: Int, height: Int, parameters: Camera.Parameters?): Camera.Size? {
        var result: Camera.Size? = null

        parameters?.let {
            for (size in it.supportedPreviewSizes) {
                if (size.width <= width && size.height <= height) {
                    if (result == null) {
                        result = size
                    } else {
                        result?.let {
                            val resultArea = it.width * it.height
                            val newArea = size.width * size.height

                            if (newArea > resultArea) {
                                result = size
                            }
                        }
                    }
                }
            }
        }

        return result
    }

    private fun getLargestPictureSize(parameters: Camera.Parameters?): Camera.Size? {
        var result: Camera.Size? = null

        parameters?.let {
            for (size in it.supportedPictureSizes) {
                if (result == null) {
                    result = size
                } else {
                    result?.let {
                        val resultArea = it.width * it.height
                        val newArea = size.width * size.height

                        if (newArea > resultArea) {
                            result = size
                        }
                    }
                }
            }
        }

        return result
    }

    private fun initPreview(width: Int, height: Int) {
        if (camera != null && previewHolder?.surface != null) {
            try {
                camera?.setPreviewDisplay(previewHolder)
            } catch (t: Throwable) {
                Log.e(TAG, "Exception in setPreviewDisplay()", t)
                Toast.makeText(this@CameraActivity, t.message,
                        Toast.LENGTH_LONG).show()
            }

            if (!cameraConfigured) {
                val parameters = camera?.parameters
                val size = getBestPreviewSize(width, height, parameters)
                val pictureSize = getLargestPictureSize(parameters)

                if (size != null && pictureSize != null) {
                    parameters?.setPreviewSize(size.width, size.height)
                    parameters?.set("jpeg-quality", 70)
                    parameters?.setPictureSize(pictureSize.width, pictureSize.height)
                    parameters?.pictureFormat = PixelFormat.JPEG

                    camera?.parameters = parameters
                    cameraConfigured = true
                }
            }
        }
    }

    private fun releaseCameraAndPreview() {
        camera?.let {
            camera?.release()
            camera = null
        }
    }

    private fun setupCamera() {
        try {
            releaseCameraAndPreview()
            val info = Camera.CameraInfo()
            for (i in 0 until Camera.getNumberOfCameras()) {
                Camera.getCameraInfo(i, info)

                if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                    camera = Camera.open(i)
                    camera?.setDisplayOrientation(90)
                    val params = camera?.parameters
                    params?.focusMode = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE
                    camera?.parameters = params

                    unexpectedTerminationHelper.init()
                }
            }
            startPreview()
        } catch (e: Exception) {
            Log.e(TAG, "setupCamera:", e)
            e.printStackTrace()
        }
    }

    private fun startPreview() {
        if (cameraConfigured && camera != null) {
            camera?.startPreview()
            inPreview = true
        }
    }

    private fun stopPreviewAndReleaseCamera() {
        camera?.let {
            if (inPreview) {
                camera?.stopPreview()
            }
            camera?.release()
            Log.i(TAG, "------------ CAMERA RELEASED ------------")
            camera = null
        }

        inPreview = false
        unexpectedTerminationHelper.fini()
    }

    private inner class UnexpectedTerminationHelper {
        private var mThread: Thread? = null
        private var mOldUncaughtExceptionHandler: Thread.UncaughtExceptionHandler? = null
        private val mUncaughtExceptionHandler = Thread.UncaughtExceptionHandler { thread, ex ->
            // gets called on the same (main) thread
            mOldUncaughtExceptionHandler?.let {
                stopPreviewAndReleaseCamera()
                // it displays the "force close" dialog
                mOldUncaughtExceptionHandler?.uncaughtException(thread, ex)
                this@CameraActivity.finish()
            }
        }

        internal fun init() {
            mThread = Thread.currentThread()
            mOldUncaughtExceptionHandler = mThread?.uncaughtExceptionHandler
            mThread?.uncaughtExceptionHandler = mUncaughtExceptionHandler
        }

        internal fun fini() {
            mThread?.let {
                mThread?.uncaughtExceptionHandler = mOldUncaughtExceptionHandler
                mOldUncaughtExceptionHandler = null
                mThread = null
            }
        }
    }

    // Private methods

    interface OnPictureTakenInterface {
        fun onPictureTakenCallback(pictureTaken: ByteArray)
    }
}

