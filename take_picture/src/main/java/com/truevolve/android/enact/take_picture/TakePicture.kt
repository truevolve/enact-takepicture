package com.truevolve.android.enact.take_picture

import android.Manifest
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import com.truevolve.policy.enact.controllers.R
import org.json.JSONException
import org.json.JSONObject

open class TakePicture : ActivityBaseController(), CameraActivity.OnPictureTakenInterface {
    private var launch_view: RelativeLayout? = null
    private var review_view: RelativeLayout? = null
    private var reviewImage: ImageView? = null
    private var bmp: Bitmap? = null
    private var pictureHasBeenTaken = false

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        pictureCallback = this

        setContentView(R.layout.activity_take_picture)

        reviewImage = findViewById(R.id.reviewView)
        launch_view = findViewById(R.id.container_launch)
        review_view = findViewById(R.id.container_review)
        val instructionText = findViewById<TextView>(R.id.instructionText)
        val launcher = findViewById<Button>(R.id.launcherView)
        val rejector = findViewById<ImageView>(R.id.rejectorView)
        val acceptor = findViewById<ImageView>(R.id.acceptorView)

        try {
            instructionText.text = stateObj?.getString(DISPLAY_MESSAGE)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        launch_view?.visibility = View.VISIBLE
        review_view?.visibility = View.GONE

        launcher.setOnClickListener {
            val permission = ContextCompat.checkSelfPermission(this@TakePicture,
                    Manifest.permission.CAMERA)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                makeRequest()
            } else {
                launchCameraActivity()
            }
        }

        rejector.setOnClickListener {
            bmp?.recycle()
            bmp = null

            launch_view?.visibility = View.GONE
            review_view?.visibility = View.GONE

            val ii = Intent(this@TakePicture, CameraActivity::class.java)
            startActivityForResult(ii, REQUESTCODE_CAMERA)
        }

        acceptor.setOnClickListener {
            launch_view?.visibility = View.GONE
            review_view?.visibility = View.GONE
            bmp?.recycle()

            try {
                stateObj?.getString(ON_TAKEN_PICTURE)?.let { goToState(it) }
            } catch (e: InterpreterException) {
                e.printStackTrace()
                Interpreter.error(this@TakePicture, e)
            } catch (e: JSONException) {
                e.printStackTrace()
                Interpreter.error(this@TakePicture, e)
            }
        }

    }

    private fun launchCameraActivity() {
        val ii = Intent(this@TakePicture, CameraActivity::class.java)
        startActivityForResult(ii, REQUESTCODE_CAMERA)
    }

    override fun onBackPressed() {
        if (review_view?.visibility == View.VISIBLE) {
            launch_view?.visibility = View.VISIBLE
            review_view?.visibility = View.GONE
            bmp?.recycle()
        } else {
            super.onBackPressed()
        }
    }

    public override fun onResume() {
        super.onResume()

        if (pictureHasBeenTaken && reviewImage != null) {
            launch_view?.visibility = View.GONE
            review_view?.visibility = View.VISIBLE
            reviewImage?.setImageBitmap(bmp)
        }
    }

    private fun rotateImage(src: Bitmap?, degree: Float): Bitmap? {
        src?.let {
            val matrix = Matrix()
            matrix.postRotate(degree)
            this.bmp = null
            return Bitmap.createBitmap(src, 0, 0, src.width, src.height, matrix, true)
        }
    }

    override fun onPictureTakenCallback(pictureTaken: ByteArray) {

        pictureHasBeenTaken = true
        bmp = BitmapFactory.decodeByteArray(pictureTaken, 0, pictureTaken.size)
        bmp = rotateImage(bmp, 90f)

        try {
            stateObj?.getString(STORE_AS)?.let { Interpreter.dataStore.put(it, pictureTaken) }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CAMERA_REQUEST_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i(TAG, "Permission has been denied by user")
                } else {
                    Log.i(TAG, "Permission has been granted by user")
                    launchCameraActivity()
                }
                return
            }
        }
    }

    // Ask for Permission to use camera
    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), CAMERA_REQUEST_CODE)
    }

    override val type = CONTROLLER_TYPE

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        Log.d(TAG, "validate: starting")

        if (!stateObj.has(STORE_AS)) {
            Log.e(TAG, "validate: state object must have $STORE_AS defined")
            throw PolicyException("State object must have $STORE_AS defined")

        } else if (!stateObj.has(ON_TAKEN_PICTURE)) {
            Log.e(TAG, "validate: state object must have $ON_TAKEN_PICTURE defined")
            throw PolicyException("State object must have $ON_TAKEN_PICTURE defined")

        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $DISPLAY_MESSAGE defined")
            throw PolicyException("State object must have $DISPLAY_MESSAGE defined")
        }
    }

    companion object {
        private val STORE_AS = "store_as"
        private val ON_TAKEN_PICTURE = "on_taken_picture"
        private val DISPLAY_MESSAGE = "display_message"

        private val CONTROLLER_TYPE = "take_picture"

        private val REQUESTCODE_CAMERA = 2343
        private val CAMERA_REQUEST_CODE = 348
        var pictureCallback: CameraActivity.OnPictureTakenInterface? = null
    }
}