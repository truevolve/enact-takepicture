package enact.technology.truevolve.sample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.truevolve.android.enact.take_picture.TakePicture
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import java.util.*

class MainActivity : AppCompatActivity() {
    private val policy = "{\n" +
            "  \"start\": {\n" +
            "    \"type\": \"menu\",\n" +
            "    \"on_take_picture\": \"take_picture\"\n" +
            "  },\n" +
            "  \"take_picture\": {\n" +
            "    \"type\": \"take_picture\",\n" +
            "    \"store_as\": \"picture_bytes\",\n" +
            "    \"on_taken_picture\": \"summary\",\n" +
            "    \"display_message\": \"Take a picture of your keyboard.\"\n" +
            "  },\n" +
            "  \"summary\": {\n" +
            "    \"type\": \"summary\",\n" +
            "    \"on_done\": \"end\",\n" +
            "    \"picture_data\": \"picture_bytes\",\n" +
            "    \"display_message\": \"This is your photo: \"\n" +
            "  }\n" +
            "}"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (intent.hasExtra(Interpreter.END)) {
            Log.d(TAG, "onCreate: ENDED")
        }
        if (intent.hasExtra(Interpreter.ERROR)) {
            Log.d(TAG, "onCreate: ERRORED")
            val throwable = intent.getSerializableExtra("exception") as Throwable
            Log.e(TAG, "onCreate: Errored: ", throwable)
        }

        val listOfControllers = ArrayList<Class<out ActivityBaseController>>()
        listOfControllers.add(Menu::class.java)
        listOfControllers.add(TakePicture::class.java)
        listOfControllers.add(Summary::class.java)

        try {
            val interpreter = Interpreter.setup(MainActivity::class.java, policy, listOfControllers)
            interpreter.start(this)

        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: PolicyException) {
            e.printStackTrace()
        } catch (e: InterpreterException) {
            e.printStackTrace()
        } catch (e: InstantiationException) {
            e.printStackTrace()
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        }

    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: RESUMING")
    }

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }
}
