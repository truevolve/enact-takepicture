package enact.technology.truevolve.sample

import android.os.Bundle
import android.util.Log
import android.widget.Button
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONException
import org.json.JSONObject

class Menu : ActivityBaseController() {

    override val type = CONTROLLER_TYPE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        try {
            val imageButton = findViewById<Button>(R.id.startProcessButton)
            imageButton.setOnClickListener {
                try {
                    stateObj?.getString(ON_TAKE_PICTURE)?.let { goToState(it) }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Interpreter.error(this@Menu, e)
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(ON_TAKE_PICTURE)) {
            Log.e(TAG, "validate: state object does not have $ON_TAKE_PICTURE which is mandatory")
            throw PolicyException("State object does not have $ON_TAKE_PICTURE which is mandatory")
        }
    }

    companion object {
        private const val ON_TAKE_PICTURE = "on_take_picture"
        private const val TAG = "Menu"
        private const val CONTROLLER_TYPE = "menu"
    }
}
