package enact.technology.truevolve.sample

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import org.json.JSONObject

class Summary : ActivityBaseController() {

    // Override methods

    override val type = CONTROLLER_TYPE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_summary)

        val done = findViewById<View>(R.id.doneButton) as Button
        val reviewPicture = findViewById<View>(R.id.takenPicture) as ImageView

        try {
            val pictureData: ByteArray? = stateObj?.getString(PICTURE_DATA)?.let { Interpreter.dataStore.getProperty(it) }

            if (pictureData != null) {
                var bmp = BitmapFactory.decodeByteArray(pictureData, 0, pictureData.size)
                bmp = rotateImage(bmp, 90f)
                reviewPicture.setImageBitmap(bmp)
                done.setOnClickListener {
                    try {
                        stateObj?.getString(ON_DONE)?.let { goToState(it) }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Interpreter.error(this@Summary, e)
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Throws(PolicyException::class)
    override fun validate(stateObj: JSONObject) {
        Log.d(TAG, "validate: starting")

        if (!stateObj.has(ON_DONE)) {
            Log.e(TAG, "validate: state object must have $ON_DONE defined")
            throw PolicyException("State object must have $ON_DONE defined")

        } else if (!stateObj.has(PICTURE_DATA)) {
            Log.e(TAG, "validate: state object must have $PICTURE_DATA defined")
            throw PolicyException("State object must have $PICTURE_DATA defined")

        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $DISPLAY_MESSAGE defined")
            throw PolicyException("State object must have $DISPLAY_MESSAGE defined")
        }
    }

    // Override methods
    // Private methods

    private fun rotateImage(src: Bitmap, degree: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(degree)
        return Bitmap.createBitmap(src, 0, 0, src.width, src.height, matrix, true)
    }

    // Private methods

    companion object {
        private const val TAG = "Summary"
        private const val ON_DONE = "on_done"
        private const val PICTURE_DATA = "picture_data"
        private const val DISPLAY_MESSAGE = "display_message"
        private const val CONTROLLER_TYPE = "summary"
    }
}
